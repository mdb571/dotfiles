export PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\e[1;32m \u@\h:\w\$\e[m"
export DEBEMAIL=mdbilal@disroot.org
export DEBFULLNAME='Mohammed Bilal'
alias lintian='lintian -iIEcv --pedantic --color auto'
alias git-import-dsc='git-import-dsc --author-is-committer --pristine-tar'
alias clean='fakeroot debian/rules clean'
alias gc='git commit -m'
alias gp='git push -u --all --follow-tags'
alias import-pt='gbp import-dsc --pristine-tar'
alias dbu='dpkg-buildpackage -us -uc'
export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig

PATH="/home/rmb/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/rmb/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/rmb/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/rmb/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/rmb/perl5"; export PERL_MM_OPT;
